Mysql and phpmyadmin for devs.
----------------

  - create `.env` file under `\mysql\5.5`, `\mysql\5.7`
    * You can override the mysql root password in the `.env` file.


  - You can then run `make mysql55`,  `make mysql57` or `make both` to run containers with mysql 5.5, 5.7 or both respectively.
  
  
Mysql containers
----------------

MYSQL containers are created for versions 5.5, 5.6 and 5.7 of MYSQL, based on the official MYSQL docker images.

Each MYSQL container is accessible from the host (address 127.0.0.1). Each container uses a different port on the host:

| Container  | Version  | Port on host | Port on container  |
| ---------- | -------- | ------------ | ------------------ |
| mysql55    | 5.5      | 3355         | 3306               |
| mysql57    | 5.7      | 3307         | 3306               |


The mysql configuation is set under `mysql/{version}/conf.d`.

The mysql data is stored under `mysql/{version}/lib`.
  
PhpMyAdmin
----------

A container for phpmyadmin container is also created

| Container  | Port on host | Port on container  |
| ---------- |  ------------ | ------------------ |
| phpmyadmin    |  8888        | 80               |


You can access phpmyadmin using url `127.0.0.1:8888` Or `http://www.pma.test` if you have www.pma.test pointed to you localhost
