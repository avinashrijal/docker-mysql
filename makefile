mysql57:
	- docker-compose down
	- docker-compose -f mysql57-docker-compose.yml up -d
mysql55:
	- docker-compose down
	- docker-compose -f mysql55-docker-compose.yml up -d
both:
	- docker-compose down
	- docker-compose up -d

network:
	- docker network create local_default

